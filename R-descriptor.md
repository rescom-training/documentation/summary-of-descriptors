![ unimelb_logo_rescomp_R.jpg](/images/unimelb_logo_rescomp_R.jpg)
# About this course

The goal of this workshop is to teach novice programmers to write code in R for data analysis. R is commonly used in many scientific disciplines for statistical analysis and its array of third-party packages. The emphasis of these materials is to give attendees a strong foundation in the fundamentals of R, and to teach them how to use the packages from Tidyverse to manipulate, analyse and visualise data.
Note that this workshop will focus on teaching the fundamentals of the programming language R, and will not teach statistical analysis. If you would like help with your statistical analysis, you can contact the Statistical Consulting Centre for one-on-one consultations.
A variety of third party packages are used throughout this workshop. These are not necessarily the best, nor are they comprehensive, but they are packages we find useful, and have been chosen primarily for their usability.

Learn more about the tool itself, with this video: [(if the embed doesn't appear, click here)](https://www.youtube.com/embed/Cqtf2iz5yYE)<iframe width="560" height="315" src="https://www.youtube.com/embed/Cqtf2iz5yYE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# Learning Objectives

* Use the RStudio interface effectively
* Understand the different data types and structures used in R
* Import, manipulate and clean data
* Obtain simple summary statistics from data
* Visualise data in a meaningful and elegant way


# Learning Outcomes

* Generate reproducible analysis of quantitative data using statistical and visualisation tools
* Adapt existing computational methods and tools to complete a target task
* Engage with unfamiliar data problems and identify relevant solution strategies to solve them



# Prerequisites 

This course is aimed at beginner users of R and Rstudio - those who have never picked up the tools before, and those who may have used them in the past, but would like a refresher.
You don't need to have any previous knowledge of the tools that will be presented at the workshop. However, if you have no previous programming experience, and/or start to feel a little lost along the way, we recommend that you consult these materials for a gentle introduction to some core programming concepts.

# How is this taught?
The training is broken down into 4 introductory modules, taught online via zoom. To sign up, you can head to the [Research Computing Services Eventbrite page.](rescomunimelb.eventbrite.com) 

# What do I need?
This course is designed to get you into the Rstudio environment and working with it as you learn. To do this you will need:

A laptop running Windows 10 or macOS
An internet connection (although if you've made it this far, that shouldnt be a problem)
A copy of R and RStudio installed on your laptop - don't worry, we're about to show you how to set that up on the next page