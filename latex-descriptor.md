![unimelb_logo_rescomp_latex.jpg](images/unimelb_logo_rescomp_latex.jpg)


#LaTeX Training overview

> * Have you ever tried to write a large document, only to find your conventional word processor can’t keep up? 
> * Are you looking for a better way to handle your references? 
> * Do you have special typesetting requirements, such as heavy use of mathematical notation, and you need it to look professional?

LaTeX is a mark-up language focused on beautiful typesetting, and handles large, complex documents with ease. It is a standard tool in scientific and technical research fields, and is well-suited to the needs of people in the humanities.

This video is a great introduction to the tool and some basic fundamentals in typesetting formulas: (if the embed doesn't appear, [click here](https://www.youtube.com/embed/kOVZy6qG890))
<iframe width="560" height="315" src="https://www.youtube.com/embed/kOVZy6qG890" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Learning objectives

This training aims to enable students to:

* [ ] prepare and structure large, complex documents;
* [ ] think critically about layout for articles, reports and theses;
* [ ] generate figures, lists and tables in a document;
* [ ] submit articles for publication in the appropriate format;
* [ ] handle references and input in-text citations and a reference list;
* [ ] collaborate with other LaTeX users.

## Learning outcomes

* Analytical skills: the ability to construct and express logical arguments and to work in abstract or general terms to increase the clarity and efficiency of analysis;
* Collaborative skills: the ability to work in a team;
* Computational skills: the ability to adapt existing computational methods and tools to complete a target task;
* Problem solving skills: the ability to engage with unfamiliar problems and identify relevant solution strategies.

## About the trainer

Your LaTex Research Community Coordinator & Trainer is Meirian Lovelace-Tozer. 
She is a teacher, consultant and senior community co-ordinator at the University of Melbourne. She offers training in Ubuntu and LaTeX at Research Computing Services. My other areas of interest include data analysis, operations research and statistics, which I also have the pleasure of teaching at the University. I love teaching and am enthusiastic about enabling others to benefit from the skills that I teach!
Get in touch with her for more information about training, events, and community support:

Twitter: Follow or tweet at me on @MeirianLT and @ResCom_unimelb.
YouTube: Watch some of our videos and subscribe to the ResCom Team!
Instagram: Check out our photos and gifs on the ResCom_unimelb Instagram.
Email: Get in contact with me at: mlovelace(at)unimelb.edu.au
(Replace the "(at)" with the "@" symbol).

# Training Format

## The current format

Whilst we are practicing social distancing, we will be hosting Zoom sessions. These sessions will include a pitch (see below), where you will learn one of the key skills usually taught in a training.

* Duration: 1 hour.
* Mode: online, using Zoom.
* Frequency: monthly.

## The pitch

A short 30 minute session introducing you to LaTeX. The pitch provides an opportunity to gain some basic skills, try out the tool, and engage in a team challenge.

* Duration: 30 minutes.
* Mode: typically in-person.
* Frequency: typically the pitch is presented bi-annually at key events such as Researcher Connect, and Graduate Researcher orientation days.

## The training

A full introductory workshop, which explains the key concepts required to typeset a thesis or article. You will leave with a LaTeX template that you can adapt for your own work.

* Duration: 5 hours.
* Mode: blended, including in-person workshop supplemented by online content.
* Frequency: monthly.

## The meet-up

An opportunity to ask your burning documentation questions, learn some tips and tricks, see some exemplary documents and get to know the community that is actively using LaTeX to document their research. Advanced topics covered in these events include: mathematical typesetting, creating vector graphics using TikZ, referencing and reference management. Also known as a data lab, hacky hour or drop-in.

* Duration: 2 hours.
* Mode: in-person.
* Frequency: monthly.