![Ubuntu header](/images/unimelb_logo_rescomp_ubuntu.jpg)

# Training overview

* Are you concerned about the vulnerability of your computer to viruses and malicious software? 
* Can your computer hardware keep up with the processes you are running?
* Are you looking for a reliable alternative to avoid system crashes and slowdowns? 
* Do you want to use free and open source software?

Ubuntu is a free and open source Linux distribution. The Linux operating system runs on any hardware, is highly secure, very stable and is not prone to crashes.

This training will provide attendees with their own virtual machine. We provide a gentle introduction to basic Ubuntu usage, including logging into your virtual machine, basic commands in the terminal, installing software on Ubuntu, and transferring data to and from your virtual machine.

## Learning objectives

This training aims to enable students to:

* [ ] familiarise themselves with the Ubuntu desktop and find applications such as Git, LaTeX, MATLAB, Python, RStudio and a text editor;
* [ ] use basic terminal commands to navigate directories (folders) and files;
* [ ] install software using terminal commands;
* [ ] access data on their provided virtual machine.

## Learning outcomes

* Analytical skills: the ability to construct and express logical arguments and to work in abstract or general terms to increase the clarity and efficiency of analysis;
* Computational skills: the ability to adapt existing computational methods and tools to complete a target task;
* Problem solving skills: the ability to engage with unfamiliar problems and identify relevant solution strategies.

## The current format

Whilst we are practicing social distancing, we will be hosting Zoom sessions, with training broken down into short modules.

* Duration: 1 hour.
* Mode: online, using Zoom.
* Frequency: monthly.

## The pitch

A short 30 minute session introducing you to the command line. The pitch provides an opportunity to gain some basic skills, try out the tool, and engage in a team challenge.

* Duration: 30 minutes.
* Mode: typically in-person.
* Frequency: typically the pitch is presented bi-annually at key events such as Researcher Connect, and Graduate Researcher orientation days.

## The training

A full introductory workshop, which explains the key concepts required to get started on Ubuntu and use basic command line.

* Duration: 2 hours.
* Mode: blended, including in-person workshop supplemented by online content.
* Frequency: monthly.

## The meet-up

An opportunity to ask questions, learn some tips and tricks, see some exemplars and get to know the community that is actively using Ubuntu. Also known as a data lab, hacky hour or drop-in.

* Duration: 1 hour.
* Mode: typically in-person.
* Frequency: monthly.

## Expectations
Throughout this training, you may be asked to:

*  Watch video content or read through short structured material introducing examples and covering fundamental topics, concepts and ideas.
*  Read short resources explaining material in further depth.
*  Work collaboratively with your peers.
*  Self-assess your progress and understanding of concepts.
*  Provide feedback to improve the course.

