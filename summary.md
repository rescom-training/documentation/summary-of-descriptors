#summary

* [LaTex](latex-descriptor.md)
* [Python](python-descriptor.md)
* [3D Slicer](3Dslicer-descriptor.md)
* [R studio](R-descriptor.md)
* [Nvivo](nvivo-descriptor.md)
* [Ubuntu](ubuntu-descriptor.md)
* [3D Data Analysis](rhino-descriptor.md)
* [MATLAB](matlab-descriptor.md)
