![MATLAB HEADER](/images/unimelb_logo_rescomp_MATLAB.jpg)


# Training overview

* What is MATLAB and what can it do for you?

MATLAB is a numerical computing environment and programming language that was designed to be interactive, intuitive and able to deal with large datasets. Because of its lineage, MATLAB is mostly used in science and engineering communities to do things such as matrix manipulations. How is this useful? Many datasets come as matrices that can be manipulated in MATLAB. For example, large Excel spreadsheets and image files can be converted into matrices. MATLAB allows you to work with these matrices efficiently and effectively.
There has recently been growing interest from researchers across other faculties (Arts, Economics, Medicine and Psychology) who want to use MATLAB for data analysis and visulatisation.

Watch the short YouTube-video with the title "MATLAB - What choice will you make?" to gain more insight ([click here](https://www.youtube.com/watch?v=iqv3kV-uQNM)).
<!-- blank line -->
<figure class="video_container">
  <iframe width="1280" height="720" src="https://www.youtube.com/embed/iqv3kV-uQNM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>
<!-- blank line -->

* What will be covered in MATLAB trainings?

The MATLAB modules will teach you how to write codes in MATLAB for (Big) Data Processiong, Statistical Analysis, Plotting, Documentations, and High-Performance Computing.

## Learning objectives

This training aims to enable students to write codes in MATLAB for data processiong, analysis and visualisation. The modules will concentrate on following objectives: 

* [ ] Simple coding in MATLAB using scripts
* [ ] Familiarity with data types in MATLAB
* [ ] Indexing 
* [ ] Logical operators
* [ ] Applying if-statements and for-loops
* [ ] Writing functions
* [ ] Working with tabular data in MATLAB
* [ ] Categorising (tabular) data in MATLAB
* [ ] Working around missing data in MATLAB
* [ ] Functions and visualisation techniques useful in (descriptive) statistics
* [ ] Curve fitting and trend analysis
* [ ] Discovering Interpolation methods
* [ ] Understanding Graphics Object Hierarchy (GOH)
* [ ] Using GOH and Objects Handles to customise plots
* [ ] Learning optimum ways to save plots for reports and publications
* [ ] Compatibility of MATLAB and LaTex
* [ ] Working with NetCDF files in MATLAB
* [ ] Using the Spartan system for High-Performance Computing 



## Learning outcomes

* Analytical and problem-solving skills: By working on challenges
* Programming and computational skils: By following the training and provided resources 
* Collaborative and communication skills: By participating in groupworks and providing help to your peers once you have learned the basics

# Training Format

<h3> Current format </h3>

Whilst we are practicing social distancing, we will be hosting Zoom sessions. These sessions will include a mini-training or pitch, followed by a meet-up (see descriptions below).

* Duration: one to two hours.
* Mode: online, using Zoom.
* Frequency: weekly.

<h3> The pitch </h3>

A short 30 minute session introducing you to MATLAB. The pitch provides an opportunity to gain some basic skills, try out the tool, and engage in a team challenge.

* Duration: 30 minutes.
* Frequency: typically the pitch is presented bi-annually at key events such as Researcher Connect, and Graduate Researcher orientation days.

<h3> The (mini) training </h3>

A series of modules delivered on a weekly basis covering a particular learning objective. 

* Duration: 1 hours (online); 3-4 hours face-to-face.
* Mode: blended, including in-person workshop supplemented by online content.
* Frequency: weekly (online); 

<h3> The meet-up/Hacky Hour </h3>

An opportunity to ask questions, learn some tips and tricks, see some exemplars and get to know the community that is actively using MATLAB. Also known as a data lab, hacky hour or drop-in.

* Duration: one to two hours.
* Mode: Online
* Frequency: bi-monthly


## Dates and times 

The mini trainings are delivered via zoom. You can see when they are running and[ sign up on the Research Computing Services Eventbrite page. ](rescomunimelb.eventbrite.com)

## Eligibility and Requirements

## What do you need to know before you come?

**Participants must bring a laptop with access to MATLAB (the newest version if possible) and a charger.**

To install MATLAB using the university license, please follow the instructions [here](https://github.com/resbaz/lessons/blob/master/matlab/unimelb_matlab_install.md), or [here](https://studentit.unimelb.edu.au/software). For technical issues and support you can contact the [Student IT Centre](https://studentit.unimelb.edu.au/) or click [here](https://online.unimelb.edu.au/support/current-students/apps-and-software-info/systems-and-support). To use MATLAB from home or off campus, you need to install a VPN to 'tunnel' into the campus network. You can find more information [here](https://studentit.unimelb.edu.au/wireless-vpn/vpn).

Alternatively (in case you can't install MATLAB on your laptop, or use it remotely), you can use the online version of MATLAB by following step 1 and 2 listed [here](https://github.com/resbaz/lessons/blob/master/matlab/unimelb_matlab_install.md). You can find more information about the MATLAB online version [here](https://au.mathworks.com/products/matlab-online.html). To upload files from desktop to the cloud, please go to [MATLAB Drive](https://drive.matlab.com).

The trainings starts on a beginner’s level (no prior knowledge of MATLAB is required) and moves to data processing, statistics and plotting on an intermediate level (prior knowledge of MATLAB is required; Note: Not suitable for advanced programmers, except you want to repeat the concepts listed in [Training overview](overview.md)).

As mentioned above, you don’t need to have any previous knowledge of the tools that will be presented at the workshop. However, if you have no previous programming experience, we recommend that you consult the course materials for [introduction to programming concepts](https://nikkirubinstein.gitbooks.io/resguides-introductory-programming-concepts/content/content/welcome-to-coding.html) prior to attending the current workshop.

**My trainings are accessible, engaging and encouraging, as I’m passionate about giving other researchers the ability to help themselves. Be curios, motivated, and willing to participate and communicate with other students/researchers in an open, generous and respectful way.**

So,

* Be prepared and motivated,
* Participate and communicate,
* Be respectful, and share your knowledge (if applicable).

## Please bring with you:

* [ ] a laptop with internet access and a web browser;
* [ ] a laptop charger;
* [ ] and if you are attending one of our Zoom workshops, you must download Zoom. Instructions on how to do this are provided here: [https://lms.unimelb.edu.au/students/student-guides/zoom](https://lms.unimelb.edu.au/students/student-guides/zoom).


## Resources

**Other helpful resources are:**\
Websites for serious programming or math/numerical questions:\
https://stackoverflow.com/ \
https://math.stackexchange.com/ 

An overview of MathWorks facilities for academia: https://www.mathworks.com/academia.html

Distance Learning community (MathWorks is building this out now) - https://www.mathworks.com/matlabcentral/topics/distance-learning.html \
Websites for interaction with other MATLAB users: https://au.mathworks.com/matlabcentral/answers/ \
Blogs - https://blogs.mathworks.com/ \
Cody (is a MATLAB problem-solving game) - https://www.mathworks.com/matlabcentral/cody \
File exchange - https://au.mathworks.com/matlabcentral/fileexchange/ 

**Self-Paced Online Courses -** https://matlabacademy.mathworks.com/ \
MATLAB Documentation - https://au.mathworks.com/help/index.html 

Formal certification program -  https://www.mathworks.com/services/training/certification.html \
Full training catalog - https://www.mathworks.com/services/training.html \
Using MATLAB for research in public clouds - https://www.mathworks.com/solutions/cloud.html

Wikibook for MATLAB Beginners - https://en.wikibooks.org/wiki/MATLAB_Programming



