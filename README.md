# Research Computing Services Training Descriptors

![ unimelb_logo_rescomp.jpg](images/unimelb_logo_rescomp.jpg)

These training descriptors outline the key information about how each of our trainings run. They include the following information: <br>
* Overview 
* What you will learn 
* Eligibility and Requirements 
* Assessment
* Course Structure 
* About the trainer

<h3> Training calendar </h3>

To view our training calendar and to sign up for training, [go to our Eventbrite page here.](rescomunimelb.eventbrite.com)

<h3> Our community model </h3> 

Our training service runs on a community model. This means we do not operate a traditional helpdesk for 1-1 consultations. We run trainings where researchers are encouraged to work together on challenges, after being taught a concept, which allows them to get hands-on experience with the tool. If they need further help after training, they are encouraged to attend a hacky hour or meetup, where they can meet other researchers with more experience in the tool & they can troubleshoot together to work through the problems they are facing. It's a terrific way for not only researchers to grow their networks through these community of practices, but very efficient way to also get the help they need.  

<h3> Expectations in every training </h3>

<h5> Participants may be asked to do the following in our trainings: </h5>

* Watch video content or read through short structured material introducing examples and covering fundamental topics, concepts and ideas.
* Read short resources explaining material in further depth.
* Work collaboratively with your peers.
* Self-assess your progress and understanding of concepts.
* Provide feedback to improve the course.
