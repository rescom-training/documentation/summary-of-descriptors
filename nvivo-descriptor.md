![Nvivo header](/images/unimelb_logo_rescomp_nvivo.jpg)

# Course Description
NVivo is a digital research tool which can assist qualitative and mixed-methods researchers. If you are working with unstructured data such as interview transcripts, speeches, videos, or even twitter data, then NVivo can help save time during your research.
In this course, you will learn how NVivo can assist you with your research projects. You will understand how NVivo is embedded within your broader research methodology. This course is free to University of Melbourne researchers.

Want to learn more about the tool? This fun and informative video has more information: [(click here if the embed doesn't appear) ](https://www.youtube.com/watch?v=EEx78BbzcSs)

<iframe width="560" height="315" src="https://www.youtube.com/embed/EEx78BbzcSs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# Training Format
This course is separated into individual modules. Whilst the modules are designed to be accessed during an online learning workshop (via Zoom).
Each module consists of four sections with an associated challenge and learning objective attached. The online training sessions will run for around 40 minutes with 20 minutes for unstructured questions with the trainer. 

<h3> The modules are as follows: </h3>

* Module 1: Introduction to NVivo

* Module 2: Organising Nodes and Coding Cases in NVivo

* Module 3: Keeping Track of Your Research in NVivo

* Module 4: Creating Visualisations in NVivo

* Module 5: How to Write a Literature Review in NVivo

* Module 6: Web Scraping and NVivo

# Learning Objectives
Students who have completed this course will understand how to:
* Manage a research database in NVivo
* Create and iterate on a coding framework in NVivo
* Generate a range of visualisations underpinned by data analysis
* Use NVivo for specialised research methodologies for literature reviews and web scraping


# Intended Learning Outcomes

Students who have completed this course should have acquired:

* Computational Skills: The ability to adapt existing computational methods and tools to complete a target task
* Problem Solving Skills: The ability to engage with unfamiliar problems and identify relevant solution strategies
* Analytical Skills: The ability to construct and express logical arguments and to work in abstract or general terms to increase the clarity and efficiency of analysis
* Critical Thinking Skills: The ability to critically evaluate both qualitative and quantitative data

# What do you need to know about Nvivo before you come?
No prior knowledge of NVivo is required.
Our trainings are accessible and encourage participation. We want you to come in curious and willing to meet other researchers!
* Be prepared: be ready and able to be involve yourself in learning.
* Engage: actively participate in the community and course materials.
* Think critically: analyse materials presented to form reasonable judgements.*
* Respect others: listen to and be considerate of others; practice gratitude.

# Recommended Background Knowledge
Students should have familiarity with qualitative and mixed-methods research methods. A bibliography is provided with literature on qualitative and mixed-methods research.
Participation Expectations

# Please bring with you:
A computer with access to the internet and a web browser.
A copy of NVivo installed on your computer.
and if you are attending one of our Zoom workshops, you must download Zoom. Instructions on how to do this are provided here: https://lms.unimelb.edu.au/students/student-guides/zoom.
*The University of Melbourne provides a free 12 month license for NVivo for both staff and students. It is renewed annually. You can only have one copy of NVivo per staff/student account, so make sure that you choose your device wisely.

# Expectations during training

Throughout this training, you may be asked to:
* Watch video content or read through short structured material introducing examples and covering fundamental topics, concepts and ideas.
* Read short resources explaining material in further depth.
* Work collaboratively with your peers.
* Self-assess your progress and understanding of concepts.
* Provide feedback to improve the course.


For more information, visit this link: http://unimelb.libguides.com/stat_software/nvivo
