![header image](images/unimelb_logo_rescomp_3d_data_analysis.jpg)

## Training Overview

This course is separated into individual modules & drop in sessions. The modules are designed to be accessed during an online learning workshop (via Zoom). See below regarding training times and availability.

## Training Formats

<h2> Training Modules </h2>

<h3> 3D Data Analysis for Research: Introduction to Rhino </h3>
This simple online introduction to Computer Aided Design will give you the skills to design your first basic 3D shape using Rhino! In this session you will be participating in an interactive challenge that will teach show you the basics of Rhino. Before attending please read over the resources on the gitbook here: https://ejong.gitbook.io/3d-data-for-researchers/learning-material/rhino3d Please also have a copy of Rhino installed on your device before the session, can you find a guide here:  https://ejong.gitbook.io/3d-data-for-researchers/learning-material/rhino3d


<h3> Drop in sessions </h3>
Meet up with other researchers and students using 3D printing,  skill share and get advice on how to create or improve your 3D models for printing. Newcomers and experts all welcomed! Bring along your models and work on it in a collaborative environment.


# Learning Objectives (Introduction to Rhino & Drop in Sessions)
* I understand what 3d data is. 
* I understand how to draw a shape in Rhino. 
* I understad how to trim a shape in Rhino. 
*I understand the community structure and resources available to me regarding 3d printing.
* I obtained a good overview of 3D printing workflow.

# Eligibility and Requirements

Our trainings are accessible and encourage participation. We want you to come in curious and willing to meet other researchers!
* Be prepared: be ready and able to be involve yourself in learning.
* Be engaged: actively participate in the community and course materials.
* Think critically: analyse materials presented to form reasonable judgements.
* Respect others: listen to and be considerate of others; practice gratitude.

Prior knowledge required:
* No prior knowledge of Rhino or 3D modelling is required to attend this course. 


<h3> Please bring with you: </h3>

* A computer with access to the internet and a web browser.
* A copy of the relevant tool installed on your computer. You can find find installation guides here: https://resbaz.github.io/resbook-installation/
* if you are attending one of our Zoom workshops, you must download Zoom. Instructions on how to do this are provided here: https://lms.unimelb.edu.au/students/student-guides/zoom.
* A phone or tablet as a secondary device.

# Dates and times

To find out when training is happening and to sign up, [go to our Eventbrite page here. ](rescomunimelb.eventbrite.com)

